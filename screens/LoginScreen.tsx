import React, { useEffect, useState } from "react";
import { useNavigation } from "@react-navigation/core";
import { showMessage } from "react-native-flash-message";

import {
  KeyboardAvoidingView,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import { auth } from "../firebase";

export interface IUser {
  email: string;
  password: string;
}

export interface IUser {
  email: string;
  password: string;
}

const LoginScreen = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [user, setUser] = useState(false);

  const navigation: string = useNavigation();

  useEffect(() => {
    if (user) {
      setUser(false);
    }
  }, [user]);

  const handleSignUp = (): void => {
    auth
      .createUserWithEmailAndPassword(email, password)
      .then((userCredentails) => {
        const user: any =userCredentails.user;
        console.log("register : ", user.email);
        showMessage({
          description: user.email,
          message: "Registered Successfully",
          type: "success",
        });
        setUser(true);
      })
      .catch((error: { message: string }) => {
        alert(error.message);
      });
  };

  const handleLogin = (): void => {
    auth
      .signInWithEmailAndPassword(email, password)
      .then((userCredentails) => {
        let user:any = userCredentails.user;
        console.log("login :", user.email);
        navigation.replace("Root", "");
        showMessage({
          description: user.email,
          message: `Welcome ${user.email}`,
          type: "success",
        });
      })
      .catch((error: { message: any }) => {
        alert(error.message);
      });
  };

  return (
    <KeyboardAvoidingView
      style={styles.container}
      behavior={Platform.OS === "ios" ? "padding" : "height"}
    >
      <View>
        <Text style={styles.header}>todoAPP📋</Text>
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          placeholder="Email"
          value={email}
          onChangeText={(text) => setEmail(text)}
          style={styles.input}
        />
        <TextInput
          placeholder="Password"
          value={password}
          onChangeText={(text) => setPassword(text)}
          style={styles.input}
          secureTextEntry
        />
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity onPress={handleLogin} style={styles.button}>
          <Text style={styles.buttonText}>Login</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={handleSignUp}
          style={[styles.button, styles.buttonOutline]}
        >
          <Text style={styles.buttonOutlineText}>Register</Text>
        </TouchableOpacity>
      </View>
    </KeyboardAvoidingView>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  inputContainer: {
    width: "80%",
  },
  input: {
    backgroundColor: "white",
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 10,
    marginTop: 5,
  },
  buttonContainer: {
    width: "60%",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 40,
  },
  button: {
    backgroundColor: "gray",
    width: "100%",
    padding: 15,
    borderRadius: 10,
    alignItems: "center",
  },
  buttonOutline: {
    backgroundColor: "white",
    marginTop: 5,
    borderColor: "gray",
    borderWidth: 2,
  },
  buttonText: {
    color: "white",
    fontWeight: "700",
    fontSize: 16,
  },
  buttonOutlineText: {
    color: "gray",
    fontWeight: "700",
    fontSize: 16,
  },
  header: {
    fontSize: 40,
    color: "purple",
    alignSelf: "center",
    marginBottom: 25,
  },
});
